<?php include('../_partials/top.php') ?>

<button type="button" class="btn btn-info btn-sm" onclick="javascript:history.back()">
  <i class="fa fa-arrow-circle-left"></i> Kembali
</button>

<h1 class="page-header" align="center">Detail Kematian</h1>

<?php include('data-show.php') ?>

<h3>A. Data Warga</h3>
<table class="table table-striped">
  <tr>
    <th width="20%">NIK</th>
    <td width="1%">:</td>
    <td><?php echo $data_kematian[0]['nik_warga'] ?></td>
  </tr>
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td><?php echo $data_kematian[0]['nama_warga'] ?></td>
  </tr>
  <tr>
    <th>Tempat Lahir</th>
    <td>:</td>
    <td><?php echo $data_kematian[0]['tempat_lahir_warga'] ?></td>
  </tr>
  <tr> 
    <th>Tanggal Lahir</th>
    <td>:</td>
    <td><?php echo ($data_kematian[0]['tanggal_lahir_warga'] != '0000-00-00') ? date('d-m-Y', strtotime($data_kematian[0]['tanggal_lahir_warga'])) : '' ?></td>
  </tr>
  <tr>
    <th>Jenis Kelamin</th>
    <td>:</td>
    <td><?php echo $data_kematian[0]['jenis_kelamin_warga'] ?></td>
</tr>
  <tr>
    <th>Pekerjaan</th>
    <td>:</td>
    <td><?php echo $data_kematian[0]['pekerjaan_warga'] ?></td>
</tr>
</table>

<h3>B. Data KK </h3>
<?php
if(error_reporting(0)){
 ?> 
<table class="table table-striped">
  <tr>
    <th width="20%">No KK</th>
    <td width="1%">:</td>
    <td><?php echo $data_kk[0]['nomor_keluarga']?></td>
  </tr>
  <tr>
    <th>Alamat</th>
    <td>:</td>
    <td><?php echo ($data_kk[0]['alamat_keluarga']) ?></td>
  </tr>
  <tr>
    <th>Dusun</th>
    <td>:</td>
    <td><?php echo "-" ?></td>
  </tr>
  <tr>
    <th>RT</th>
    <td>:</td>
    <td>
      <?php echo ($data_kk[0]['rt_keluarga'] )?>
    </td>
  </tr>
  <tr>
    <th>RW</th>
    <td>:</td>
    <td><?php echo $data_kk[0]['rw_keluarga'] ?></td>
  </tr>
</table>
<?php } ?>

<h3>C. Data Alamat</h3>
<table class="table table-striped">
  <tr>
    <th width="20%">Alamat KTP</th>
    <td width="1%">:</td>
    <td><?php echo $data_kematian[0]['alamat_ktp_warga'] ?></td>
  </tr>
  <tr>
    <th>Alamat</th>
    <td>:</td>
    <td><?php echo $data_kematian[0]['alamat_warga'] ?></td>
  </tr>
  <tr>
    <th>Dusun</th>
    <td>:</td>
    <td><?php echo $data_kematian[0]['dusun_warga'] ?></td>
  </tr>
  <tr>
    <th>RT</th>
    <td>:</td>
    <td><?php echo $data_kematian[0]['rt_warga'] ?></td>
  </tr>
  <tr>
    <th>RW</th>
    <td>:</td>
    <td><?php echo $data_kematian[0]['rw_warga'] ?></td>
  </tr>
  <tr>
    <th>Desa/Kelurahan</th>
    <td>:</td>
    <td><?php echo $data_kematian[0]['desa_kelurahan_warga'] ?></td>
  </tr>
  <tr>
    <th>Kecamatan</th>
    <td>:</td>
    <td><?php echo $data_kematian[0]['kecamatan_warga'] ?></td>
  </tr>
  <tr>
    <th>Kabupaten/Kota</th>
    <td>:</td>
    <td><?php echo $data_kematian[0]['kabupaten_kota_warga'] ?></td>
  </tr>
  <tr>
    <th>Provinsi</th>
    <td>:</td>
    <td><?php echo $data_kematian[0]['provinsi_warga'] ?></td>
  </tr>
  <tr>
    <th>Negara</th>
    <td>:</td>
    <td><?php echo $data_kematian[0]['negara_warga'] ?></td>
  </tr>

</table>

<h3>D. Data Kematian</h3>
<table class="table table-striped">
    <tr>
        <th width="20%">Tanggal Kematian</th>
        <td width="1%">:</td>
        <td>
            <?= ($data_kematian[0]['tgl_meninggal'] != '0000-00-00') ? date('d-m-Y', strtotime($data_kematian[0]['tgl_meninggal'])) : '' ?>
        </td>
    </tr>
    <tr>
        <th width="20%">Sebab</th>
        <td width="1%">:</td>
        <td>
            <?= $data_kematian[0]['sebab'] ?>
        </td>
    </tr>
    <tr>
        <th width="20%">Tempat Kematian</th>
        <td width="1%">:</td>
        <td>
            <?= $data_kematian[0]['tempat_kematian'] ?>
        </td>
    </tr>
    <tr>
        <th width="20%">Nama Pelapor</th>
        <td width="1%">:</td>
        <td>
            <?= $data_kematian[0]['nama_pelapor'] ?>
        </td>
    </tr>
    <tr>
        <th width="20%">Alamat Pelapor</th>
        <td width="1%">:</td>
        <td>
            <?= $data_kematian[0]['alamat_pelapor'] ?>
        </td>
    </tr>
    <tr>
        <th width="20%">Hubungan Pelapor</th>
        <td width="1%">:</td>
        <td>
            <?= $data_kematian[0]['hubungan_pelapor'] ?>
        </td>
    </tr>
</table>


<?php include('../_partials/bottom.php') ?>
