<?php include('../_partials/top.php') ?>

<?php include('list_keluarga.php') ?>
<h1 class="page-header">Tambah Kematian</h1>
<?php include('_partials/menu.php');
include('../../config/koneksi.php');

$query3 = "SELECT * FROM warga order by id_warga DESC";
  $hasil3 = mysqli_query($db, $query3);
  $jum=mysqli_num_rows($hasil3);
if ($jum>0){
  $hasil4=mysqli_fetch_array($hasil3);
  $kode=$hasil4['id_warga']+1;
} else {
  $kode=1;

}
$query7 = "SELECT * FROM tbl_meninggal order by id_meninggal DESC";
  $hasil7 = mysqli_query($db, $query7);
  $jum7=mysqli_num_rows($hasil7);
if ($jum7>0){
  $hasil8=mysqli_fetch_array($hasil7);
  $kode2=$hasil8['id_meninggal']+1;
} else {
  $kode2=1;

}



?>
<form action="store.php" method="post">
<label class="col-md-0 control-label" for="=">
    <h4>Data warga</h4>
</label>
<legend></legend>
<div class="form-group">
    <label class="col-md-3 control-label" for="no_nama"></label>
    <div class="col-md-9">
        <span class="help-block">
            <select class="form-control selectlive" name="nama_warga" id="isian" required>
                <option value="" selected disabled>- cari -</option>
                <?php foreach ($data_warga as $warga) : ?>
                <option value="<?php echo $warga['id_warga'] ?>" id_warga="<?php echo $warga['id_warga'] ?>" nik_warga="<?php echo $warga['nik_warga'] ?>"
                    alamat_warga="<?php echo $warga['alamat_warga'] ?>" nik="<?php echo $warga['nik_warga'] ?>"
                    nama_warga="<?php echo $warga['nama_warga'] ?>" desa_warga="<?php echo $warga['desa_kelurahan_warga'] ?>"
                    dusun_warga="<?php echo $warga['dusun_warga'] ?>" rt_warga="<?php echo $warga['rt_warga'] ?>"
                    rw_warga="<?php echo $warga['rw_warga'] ?>">
                    <?php echo $warga['nama_warga'] ?> (NIK: <?php echo $warga['nik_warga'] ?>)
                </option>
                <?php endforeach ?>
            </select>
            <!--<input type="text" class="form-control" name="no_nama" id="no_nama" size="50" placeholder="No / Nama (min 2 karakter)"  />-->

        </span>
        <legend></legend>
    </div>
</div>
<input type="hidden" class="form-control input-md" name="id_warga" id="id_warga" size="50" />

<div class="form-group">
    <label class="col-md-3 control-label" for="nik_warga">NIK</label>
    <div class="col-md-9">
        <span class="help-block">
            <input id="nik_warga_sementara" name="nik_warga_sementara" type="text" placeholder="NIK"
                class="form-control input-md" required="" disabled />
            <input type="hidden" class="form-control input-md" name="nik_warga" id="nik_warga" size="50" />
            <input type="hidden" class="form-control input-md" name="nik_warga" id="nik_warga" size="50" />
        </span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label" for="nama">Nama</label>
    <div class="col-md-9">
        <span class="help-block">
            <input id="nama_sementara" name="nama_sementara" type="text" placeholder="Nama_warga"
                class="form-control input-md" required="" disabled />
            <input type="hidden" class="form-control input-md" name="nama_warga" id="nama" size="50" />
        </span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label" for="alamat_warga">Alamat</label>
    <div class="col-md-9">
        <span class="help-block">
            <input id="alamat_warga_sementara" name="alamat_warga_sementara" type="text" placeholder="Alamat"
                class="form-control input-md" required="" disabled />
            <input type="hidden" class="form-control input-md" name="alamat_warga" id="alamat_warga" size="50" />
        </span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label" for="desa_warga">Desa</label>
    <div class="col-md-9">
        <span class="help-block">
            <input id="desa_warga_sementara" name="desa_warga_sementara" type="text" placeholder="Desa"
                class="form-control input-md" required="" disabled />
            <input type="hidden" class="form-control input-md" name="desa_warga" id="desa_warga" size="50" />
        </span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label" for="dusun_warga">Dusun</label>
    <div class="col-md-9">
        <span class="help-block">
            <input id="dusun_warga_sementara" name="dusun_warga_sementara" type="text" placeholder="Dusun"
                class="form-control input-md" required="" disabled />
            <input type="hidden" class="form-control input-md" name="dusun_warga" id="dusun_warga" size="50" />
        </span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label" for="rt_warga">RT</label>
    <div class="col-md-9">
        <span class="help-block">
            <input id="rt_warga_sementara" name="rt_warga_sementara" type="text" placeholder="RT"
                class="form-control input-md" required="" disabled />
            <input type="hidden" class="form-control input-md" name="rt_warga" id="rt_warga" size="50" />
        </span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label" for="rw_warga">RW</label>
    <div class="col-md-9">
        <span class="help-block">
            <input id="rw_warga_sementara" name="rw_warga_sementara" type="text" placeholder="RW"
                class="form-control input-md" required="" disabled />
            <input type="hidden" class="form-control input-md" name="rw_warga" id="rw_warga" size="50" />
        </span>
    </div>
</div>

<input type="hidden"  name="status_warga" value="Meninggal">
<input type="hidden"  name="id_surat" value="<?php echo $kode2 ?>">


<legend>&nbsp </legend>
<label class="col-md-0 control-label" for="=">
    <h4>Data Pelapor</h4>
</label>
<legend></legend>



<div class="form-group">
    <label class="col-md-3 control-label" for="nama_pelapor">Nama Pelapor</label>
    <div class="col-md-9">
        <span class="help-block">
            <input type="text" class="form-control input-md" name="nama_pelapor" id="nama_pelapor" size="30"
                placeholder="Nama Pelapor" />
        </span>
        <?php # echo form_error('nama_pelapor', '<p class="field_error">','</p>')?>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label" for="alamat_pelapor">Alamat Pelapor</label>
    <div class="col-md-9">
        <span class="help-block">
            <input type="text" class="form-control input-md" name="alamat_pelapor" id="alamat_pelapor" size="30"
                placeholder="Alamat Pelapor" />
            <?php # echo form_error('alamat_pelapor', '<p class="field_error">','</p>')?>
        </span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label" for="hubungan_pelapor">Hubungan Pelapor</label>
    <div class="col-md-9">
        <span class="help-block">
            <input type="text" class="form-control input-md" name="hubungan_pelapor" id="hubungan_pelapor" size="30"
                placeholder="Hubungan Pelapor" />
            <?php # echo form_error('alamat_pelapor', '<p class="field_error">','</p>')?>
        </span>
    </div>
</div>

<legend>&nbsp </legend>
<label class="col-md-0 control-label" for="=">
    <h4>Keterangan</h4>
</label>
<legend></legend>



<div class="form-group">
    <label class="col-md-3 control-label" for="sebab">Penyebab Kematian</label>
    <div class="col-md-9">
        <span class="help-block">
            <input type="text" class="form-control input-md" name="sebab" id="sebab" size="30"
                placeholder="Penyebab Kematian" />
        </span>
        <?php # echo form_error('nama_pelapor', '<p class="field_error">','</p>')?>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label" for="tempat_kematian">Tempat Kematian</label>
    <div class="col-md-9">
        <span class="help-block">
            <input type="text" class="form-control input-md" name="tempat_kematian" id="tempat_kematian" size="30"
                placeholder="Tempat Kematian" />
            <?php # echo form_error('alamat_pelapor', '<p class="field_error">','</p>')?>
        </span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label" for="tanggal_kematian">Tanggal Kematian</label>
    <div class="col-md-9">
        <span class="help-block">
            <input type="date" class="form-control input-md" name="tanggal_kematian" id="tanggal_kematian" size="30"
                placeholder="Tanggal Kematian" />
            <?php # echo form_error('alamat_pelapor', '<p class="field_error">','</p>')?>
        </span>
    </div>
</div>


<legend>&nbsp </legend>
<!--
  <label class="col-md-0 control-label" for="="><h4>Pencetak Surat Kelahiran</h4></label>
  <legend></legend>
  <div class="form-group">
     <label class="col-md-3 control-label" for="alamat_pelapor">Aparat Desa</label>
    <div class="col-md-9">
      <div class="form-group">
        <select class="form-control" name="aparat_desa">
          <option>--Pilih--</option>
          <option>Kepala Desa</option>
          <option>Sekretaris Desa</option>
          <option>Kaur</option>
        </select>
      </div>

        <?php # $id = 'id="id_perangkat" class="form-control input-md" required'; echo form_dropdown('id_perangkat',$nama_pamong,'',$id)?>
    <span class="help-block"><?php #echo form_error('id_perangkat', '<p class="field_error">','</p>')?> </span> 
    </div>  
  </div>
-->
<legend></legend>
<button type="submit" class="btn btn-success">
    <i class="fa fa-save"></i> Simpan
</button>
<button type="reset" class="btn btn-warning">
    <i class="fa fa-repeat 
 "></i> Reset
</button>
</form>



<?php include('../_partials/bottom.php') ?>
<script type="text/javascript">
$("#isian").on("change", function() {
    var id_warga = $("#isian option:selected").attr("id_warga");
    var nik_warga = $("#isian option:selected").attr("nik_warga");
    var nama_warga = $("#isian option:selected").attr("nama_warga");
    var alamat_warga = $("#isian option:selected").attr("alamat_warga");
    var desa_warga = $("#isian option:selected").attr("desa_warga");
    var dusun_warga = $("#isian option:selected").attr("dusun_warga");
    var rt_warga = $("#isian option:selected").attr("rt_warga");
    var rw_warga = $("#isian option:selected").attr("rw_warga");
    //window.alert ("hehe");
    //pindah isi ke tag lain
    $("#id_warga").val(id_warga);
    $("#nik_warga").val(nik_warga);
    $("#nik_warga_sementara").val(nik_warga);
    $("#nik_warga").val(nik_warga);
    $("#nama_sementara").val(nama_warga);
    $("#nama").val(nama_warga);
    $("#alamat_warga_sementara").val(alamat_warga);
    $("#alamat_warga").val(alamat_warga);
    $("#desa_warga_sementara").val(desa_warga);
    $("#desa_warga").val(desa_warga);
    $("#dusun_warga_sementara").val(dusun_warga);
    $("#dusun_warga").val(dusun_warga);
    $("#rt_warga_sementara").val(rt_warga);
    $("#rt_warga").val(rt_warga);
    $("#rw_warga_sementara").val(rw_warga);
    $("#rw_warga").val(rw_warga);


});
</script>