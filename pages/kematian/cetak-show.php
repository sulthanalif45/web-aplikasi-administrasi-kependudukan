<?php
require_once("../../assets/lib/fpdf/fpdf.php");
require_once("../../config/koneksi.php");

class PDF extends FPDF
{
    // Page header
    function Header()
    {
      // Logo
      $this->Image('../../assets/img/logosumedang.png',20,10);

        // Arial bold 15
        $this->SetFont('Times','B',15);
        // Move to the right
        // $this->Cell(60);
        // Title
        $this->Cell(200,8,'PEMERINTAH KABUPATEN SUMEDANG',0,1,'C');
        $this->Cell(200,8,'KECAMATAN CIMANGGUNG',0,1,'C');
        $this->Cell(200,8,'DESA CIHANJUANG',0,1,'C');
        $this->SetFont('Times','',13);
        $this->Cell(200,8,'Jl.Bunter No.1 Ds.Cihanjuang, Kec.Cimanggung, Kab.Sumedang',0,1,'C');
        // Line break

        $this->SetFont('Times','BU',12);
        for ($i=0; $i < 10; $i++) {
            $this->Cell(190,0,'',1,1,'C');
        }

        $this->Ln(10);
    }

    //Page footer
    function Footer()
    {
        //atur posisi 1.5 cm dari bawah
        $this->SetY(-15);
        //Arial italic 9
        $this->SetFont('Arial','I',9);
        //nomor halaman
    
    }

        
}

// ambil dari url

// ambil dari database
include "data-show.php";
            

            

        function TanggalIndo($date){
        $BulanIndo = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");

        $tahun = substr($date,0,4);
        $bulan = substr($date,5,2);
        $tgl = substr($date, 8,2);

        $result = $tgl." ". $BulanIndo[(int)$bulan-1]." ".$tahun;
        return($result);
        }

        function nosurat($date){
        $BulanIndo = array("I","II","III","IV","V","VI","VII","VIII","IX","X","XI","XII");

        $tahun = substr($date,0,4);
        $bulan = substr($date,5,2);
        $tgl = substr($date, 8,2);

        $result = $BulanIndo[(int)$bulan];
        return($result);
    }
    $nomor_surat = $data_kematian[0]['id_surat']."/SKet/".nosurat(date('m'))."/".date('Y');
    //$NIP = "";
    $provinsi = "Jawa Barat";
    $kabupaten = "Sumedang";
    $kecamatan = "Cimanggung";
    $desa = "Cihanjuang";
    
    $jabatan="Kepala Desa";
    $nama_pamong="Yuyus Yusuf, S.Sy";

$pdf = new PDF('P', 'mm', [210, 330]);
$pdf->AliasNbPages();
$pdf->AddPage();

// set font
$pdf->SetFont('Times','',12);


            //Jenis Surat
        $pdf->SetFont('Times','U',15);
        $pdf->Cell(0,0,'SURAT KETERANGAN KEMATIAN ',0,1,'C');
            
            //Nomor Surat
            $pdf->SetFont('Times','',13);
            $pdf->Cell(0,10,'Nomor: '.$nomor_surat,0,1,'C');
            
            //Isi Surat
            $jenis = '         Yang bertanda tangan dibawah ini Kepala Desa '.$desa.', Kecamatan '.$kecamatan.', Kabupaten '.$kabupaten.', Provinsi '.$provinsi.' menerangkan dengan sebenarnya, bahwa:';
            $pdf->SetFont('Times','',11);
            $pdf->Ln(3);
            $pdf->MultiCell(0,5,$jenis,'',"L");
            
            //Pindah Baris Isi Surat
            $pdf->Ln(6);
            $pdf->Cell(0,5,'NIK',0,0,'L');
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.strtoupper($data_kematian[0]['nik_warga']),0,1,'L');

            $pdf->Cell(0,5,'Nama',0,0,'L');
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.strtoupper($data_kematian[0]['nama_warga']),0,1,'L');
            
            $pdf->Cell(0,5,'Tempat Lahir',0,0,'L');     
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.strtoupper($data_kematian[0]['tempat_lahir_warga']),0,1,'L');
            
            $pdf->Cell(0,5,'Tanggal Lahir',0,0,'L');
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.TanggalIndo($data_kematian[0]['tanggal_lahir_warga']),0,1,'L');
            
            $pdf->Cell(0,5,'Jenis Kelamin',0,0,'L');
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.strtoupper($data_kematian[0]['jenis_kelamin_warga']),0,1,'L');
            
            $pdf->Cell(0,5,'Agama',0,0,'L');
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.strtoupper($data_kematian[0]['agama_warga']),0,1,'L');
            
            $pdf->Cell(0,5,'Warga Negara',0,0,'L');
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.strtoupper($data_kematian[0]['negara_warga']),0,1,'L');
            
            $pdf->Cell(0,5,'Pekerjaan',0,0,'L');
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.strtoupper($data_kematian[0]['pekerjaan_warga']),0,1,'L');
            
            $pdf->Cell(0,5,'Alamat',0,0,'L');
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.strtoupper($data_kematian[0]['alamat_warga']),0,1,'L');

            //isi meninggal
            $jenis = 'Telah meninggal dunia pada:';
            $pdf->SetFont('Times','',11);
            $pdf->Ln(3);
            $pdf->MultiCell(0,5,$jenis,'',"L");

            $pdf->Ln(6);
            $pdf->Cell(0,5,'Tanggal',0,0,'L');
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.TanggalIndo($data_kematian[0]['tgl_meninggal']),0,1,'L');

            $pdf->Cell(0,5,'Tempat Meninggal',0,0,'L');
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.strtoupper($data_kematian[0]['tempat_kematian']),0,1,'L');
           
            $pdf->Cell(0,5,'Sebab Meninggal',0,0,'L');
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.strtoupper($data_kematian[0]['sebab']),0,1,'L');

            //isi saksi
            $jenis = 'Berdasarkan Laporan Dari:';
            $pdf->SetFont('Times','',11);
            $pdf->Ln(3);
            $pdf->MultiCell(0,5,$jenis,'',"L");
            
            $pdf->Ln(6);
            $pdf->Cell(0,5,'Nama',0,0,'L');
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.strtoupper($data_kematian[0]['nama_pelapor']),0,1,'L');

            $pdf->Cell(0,5,'Alamat',0,0,'L');
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.strtoupper($data_kematian[0]['alamat_pelapor']),0,1,'L');

            $pdf->Cell(0,5,'Hubungan',0,0,'L');
            $pdf->Cell(-155);
            $pdf->Cell(0,5,':  '.strtoupper($data_kematian[0]['hubungan_pelapor']),0,1,'L');

            //Akhir Surat
            $jenis = '         Demikian surat ini dibuat agar dapat digunakan sebagaimana mestinya. Atas perhatiannya kami ucapkan terima kasih.';
            $pdf->SetFont('Times','',11);
            $pdf->Ln(3);
            $pdf->MultiCell(0,5,$jenis,'',"L");
            
            $pdf->Ln(30);
            $pdf->Cell(134);
            $pdf->Cell(0,5,$kabupaten.', '.TanggalIndo(date('Y-m-d')),0,0,'C');
            
            $pdf->Ln(8);
            $pdf->Cell(134);
            $pdf->Cell(0,5,$jabatan.' '.$desa,0,0,'C');
            
            $pdf->Ln(5);
            
            $pdf->SetFont('Times','BU',11);
            $pdf->Ln(30);
            $pdf->Cell(134);
            $pdf->Cell(0,5,$nama_pamong,0,0,'C');
            


    $pdf->Ln(10);

$pdf->Output();
?>
