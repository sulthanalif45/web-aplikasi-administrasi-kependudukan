<?php include('../_partials/top.php') ?>

<?php # include('list_keluarga.php') ?>
<?php include('data-edit.php') ?>
<h1 class="page-header">Ubah Kematian</h1>
<?php include('_partials/menu.php') ?>
<form action="store.php" method="post">
    <label class="col-md-0 control-label" for="=">
        <h4>Data warga</h4>
    </label>
    <legend></legend>
        <div class="form-group">
            <label class="col-md-3 control-label" for="nik_warga">NIK</label>
            <div class="col-md-9">
                <span class="help-block">
                    <input type="text" class="form-control input-md" name="nik_warga" id="nik_warga" value="<?= $data_warga[0]['nik_warga'] ?>" size="50" disabled/>
                </span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="nama_warga">Nama</label>
            <div class="col-md-9">
                <span class="help-block">
                    <input type="text" class="form-control input-md" name="nama_warga" id="nama_warga" value="<?= $data_warga[0]['nama_warga'] ?>" size="50" disabled/>
                </span>
            </div>
        </div>

        <legend>&nbsp </legend>
        <label class="col-md-0 control-label" for="=">
            <h4>Data Pelapor</h4>
        </label>
        <legend></legend>



        <div class="form-group">
            <label class="col-md-3 control-label" for="nama_pelapor">Nama Pelapor</label>
            <div class="col-md-9">
                <span class="help-block">
                    <input type="text" class="form-control input-md" name="nama_pelapor" id="nama_pelapor" value="<?= $data_warga[0]['nama_pelapor'] ?>" size="30"
                     />
                </span>
                <?php # echo form_error('nama_pelapor', '<p class="field_error">','</p>')?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="alamat_pelapor">Alamat Pelapor</label>
            <div class="col-md-9">
                <span class="help-block">
                    <input type="text" class="form-control input-md" name="alamat_pelapor" id="alamat_pelapor" value="<?= $data_warga[0]['alamat_pelapor'] ?>" size="30"
                        placeholder="Alamat Pelapor" />
                    <?php # echo form_error('alamat_pelapor', '<p class="field_error">','</p>')?>
                </span>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="hubungan_pelapor">Hubungan Pelapor</label>
            <div class="col-md-9">
                <span class="help-block">
                    <input type="text" class="form-control input-md" name="hubungan_pelapor" value="<?= $data_warga[0]['hubungan_pelapor'] ?>"id="hubungan_pelapor"
                        size="30" placeholder="Hubungan Pelapor" />
                    <?php # echo form_error('alamat_pelapor', '<p class="field_error">','</p>')?>
                </span>
            </div>
        </div>

        <legend>&nbsp </legend>
        <label class="col-md-0 control-label" for="=">
            <h4>Keterangan</h4>
        </label>
        <legend></legend>



        <div class="form-group">
            <label class="col-md-3 control-label" for="sebab">Penyebab Kematian</label>
            <div class="col-md-9">
                <span class="help-block">
                    <input type="text" class="form-control input-md" name="sebab"value="<?= $data_warga[0]['sebab'] ?>" id="sebab" size="30"
                        placeholder="Penyebab Kematian" />
                </span>
                <?php # echo form_error('nama_pelapor', '<p class="field_error">','</p>')?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="tempat_kematian">Tempat Kematian</label>
            <div class="col-md-9">
                <span class="help-block">
                    <input type="text" class="form-control input-md" name="tempat_kematian" value="<?= $data_warga[0]['tempat_kematian'] ?>" id="tempat_kematian"
                        size="30" placeholder="Tempat Kematian" />
                    <?php # echo form_error('alamat_pelapor', '<p class="field_error">','</p>')?>
                </span>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="tanggal_kematian">Tanggal Kematian</label>
            <div class="col-md-9">
                <span class="help-block">
                    <input type="text" class="form-control datepicker input-md" name="tgl_kelahiran" size="30"
                 value="<?php echo $data_warga[0]['tgl_meninggal'] ?>" />
                    <?php # echo form_error('alamat_pelapor', '<p class="field_error">','</p>')?>
                </span>
            </div>
        </div>


        <legend>&nbsp </legend>
        <!--
  <label class="col-md-0 control-label" for="="><h4>Pencetak Surat Kelahiran</h4></label>
  <legend></legend>
  <div class="form-group">
     <label class="col-md-3 control-label" for="alamat_pelapor">Aparat Desa</label>
    <div class="col-md-9">
      <div class="form-group">
        <select class="form-control" name="aparat_desa">
          <option>--Pilih--</option>
          <option>Kepala Desa</option>
          <option>Sekretaris Desa</option>
          <option>Kaur</option>
        </select>
      </div>

        <?php # $id = 'id="id_perangkat" class="form-control input-md" required'; echo form_dropdown('id_perangkat',$nama_pamong,'',$id)?>
    <span class="help-block"><?php #echo form_error('id_perangkat', '<p class="field_error">','</p>')?> </span> 
    </div>  
  </div>
-->
        <legend></legend>
        <button type="submit" class="btn btn-success">
            <i class="fa fa-save"></i> Simpan
        </button>
        <button type="reset" class="btn btn-warning">
            <i class="fa fa-repeat 
 "></i> Reset
        </button>
</form>


<?php include('../_partials/bottom.php') ?>
</script>