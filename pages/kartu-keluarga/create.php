<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Kartu Keluarga</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-create.php') ?>

<button type="button" class="btn btn-info btn-sm" onclick="javascript:history.back()">
  <i class="fa fa-arrow-circle-left"></i> Kembali
</button>

<form action="store.php" method="post">
<h3>A. Data KK</h3>
<table class="table table-striped table-middle">
  <tr>
    <th width="20%">Nomor Kartu Keluarga</th>
    <td width="1%">:</td>
    <td><input type="text" class="form-control" name="nomor_keluarga" value="<?= $nomerkk.$kode.nomoracak(3)?>" hidden readonly></td>
  </tr>
  <tr>
    <th>ID Kepala Keluarga</th>
    <td>:</td>
    <td>
      <input type="hidden" name="id_warga" value="<?php echo $warga['id_warga'] ?>">
      <select class="form-control selectlive" name="id_kepala_keluarga" required>
        <option value="" selected disabled>- pilih -</option>
        <?php foreach ($data_warga as $warga) : ?>
        <option value="<?php echo $warga['id_warga'] ?>">
          <?php echo $warga['nama_warga'] ?> (NIK: <?php echo $warga['nik_warga'] ?>)
        </option>
        <?php endforeach ?>
      </select>
    </td>
  </tr>
</table>

<h3>B. Data Alamat</h3>
<table class="table table-striped table-middle">
  <tr>
    <th width="20%">Alamat</th>
    <td width="1%">:</td>
    <td><textarea class="form-control" name="alamat_keluarga"></textarea></td>
  </tr>
  <tr>
    <th>RT</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="rt_keluarga" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="01">01</option>
        <option value="02">02</option>
        <option value="03">03</option>
        <option value="04">04</option>
        <option value="05">05</option>
        <option value="06">06</option>
      </select>
  </tr>
 <tr>
    <th>RW</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="rw_keluarga" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="01">01</option>
        <option value="01">02</option>
        <option value="01">03</option>
        <option value="01">04</option>
        <option value="01">05</option>
        <option value="01">06</option>
        <option value="01">07</option>
        <option value="01">08</option>
        <option value="01">09</option>
        <option value="01">10</option>
        <option value="01">11</option>
        <option value="01">12</option>
        <option value="01">13</option>
        <option value="01">14</option>
      </select>
  </tr>
  <tr>
    <th>Dusun</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="dusun_keluarga" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="Bunter">Bunter</option>
        <option value="SBG">SBG</option>
      </select>
    </td>
  </tr>
  <tr>
    <th>Kode Pos</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="kode_pos_keluarga" value="45364" require></td>
  </tr>

  <tr>
    <th>Desa/Kelurahan</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="desa_kelurahan_keluarga" value="Cihanjuang" required></td>
  </tr>
  <tr>
    <th>Kecamatan</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="kecamatan_keluarga" value="Cimanggung" required></td>
  </tr>
  <tr>
    <th>Kabupaten/Kota</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="kabupaten_kota_keluarga" value="Sumedang" required></td>
  </tr>
  <tr>
    <th>Provinsi</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="provinsi_keluarga" value="Jawa Barat" required></td>
  </tr>
  <tr>
    <th>Negara</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="negara_keluarga" value="Indonesia" required></td>
  </tr>
</table>

<button type="submit" class="btn btn-success">
  <i class="fa fa-save"></i> Simpan
</button>
<button type="button" class="btn btn-danger" onclick="javascript:history.back();">
  <i class="fa fa-arrow-circle-left"></i> Batal
</button>
</form>

<?php include('../_partials/bottom.php') ?>
