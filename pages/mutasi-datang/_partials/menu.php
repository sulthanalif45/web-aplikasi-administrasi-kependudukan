<div class="panel panel-default">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-2">
        <a href="pindah_masuk.php" class="btn btn-success">
          <i class="fa fa-plus"></i> Mutasi Datang
        </a>
      </div>
      <div class="col-md-2">
        <a href="cetak-index.php" target="_blank" class="btn btn-primary">
          <i class="glyphicon glyphicon-print"></i> Cetak
        </a>
      </div>
    </div>
  </div>
</div>
<br>
