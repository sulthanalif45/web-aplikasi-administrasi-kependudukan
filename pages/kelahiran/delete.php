<?php
session_start();
if (!isset($_SESSION['user'])) {
  // jika user belum login
  header('Location: ../login');
  exit();
}

include('../../config/koneksi.php');

// ambil data dari form
$id_kelahiran = htmlspecialchars($_GET['id_kelahiran']);

// delete database
$query = "DELETE FROM tbl_kelahiran WHERE id_kelahiran = $id_kelahiran";

$hasil = mysqli_query($db, $query);

// cek keberhasilan pendambahan data
if ($hasil == true) {
  echo "<script>window.location.href='../kelahiran'</script>";
} else {
  echo "<script>window.alert('Data warga gagal dihapus!'); window.location.href='../kelahiran'</script>";
}
