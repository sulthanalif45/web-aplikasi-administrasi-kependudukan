<?php include('../_partials/top.php') ?>

<button type="button" class="btn btn-info btn-sm" onclick="javascript:history.back()">
  <i class="fa fa-arrow-circle-left"></i> Kembali
</button>

<h1 class="page-header" align="center">Detail Kelahiran</h1>

<?php include('data-show.php') ?>

<h3>A. Data Bayi</h3>
<table class="table table-striped">
  <tr>
    <th width="20%">NIK</th>
    <td width="1%">:</td>
    <td><?php echo $data_warga[0]['nik_warga'] ?></td>
  </tr>
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td><?php echo $data_lahir[0]['nama_bayi'] ?></td>
  </tr>
  <tr>
    <th>Tempat Lahir</th>
    <td>:</td>
    <td><?php echo $data_lahir[0]['tempat_lahir'] ?></td>
  </tr>
  <tr>
    <th>Tanggal Lahir</th>
    <td>:</td>
    <td><?php echo ($data_lahir[0]['tgl_kelahiran'] != '0000-00-00') ? date('d-m-Y', strtotime($data_lahir[0]['tgl_kelahiran'])) : '' ?></td>
  </tr>
  <tr>
    <th>Jenis Kelamin</th>
    <td>:</td>
    <td><?php echo $data_lahir[0]['jk'] ?></td>
</tr>
  <tr>
    <th>Nama Ayah</th>
    <td>:</td>
    <td><?php echo $data_lahir[0]['nama_ayah'] ?></td>
</tr>
  <tr>
    <th>Nama Ibu</th>
    <td>:</td>
    <td><?php echo $data_lahir[0]['nama_ibu'] ?></td>
</tr>
</table>

<h3>B. Data KK </h3>
<?php
if(error_reporting(0)){
 ?> 
<table class="table table-striped">
  <tr>
    <th width="20%">No KK</th>
    <td width="1%">:</td>
    <td><?php echo $data_kk[0]['nomor_keluarga']?></td>
  </tr>
  <tr>
    <th>Alamat</th>
    <td>:</td>
    <td><?php echo ($data_kk[0]['alamat_keluarga']) ?></td>
  </tr>
  <tr>
    <th>Dusun</th>
    <td>:</td>
    <td><?php echo "-" ?></td>
  </tr>
  <tr>
    <th>RT</th>
    <td>:</td>
    <td>
      <?php echo ($data_kk[0]['rt_keluarga'] )?>
    </td>
  </tr>
  <tr>
    <th>RW</th>
    <td>:</td>
    <td><?php echo $data_kk[0]['rw_keluarga'] ?></td>
  </tr>
</table>
<?php } ?>

<h3>C. Data Kelahiran</h3>
<table class="table table-striped">
    <tr>
        <th width="20%">Berat Bayi</th>
        <td width="1%">:</td>
        <td>
            <?= $data_lahir[0]['berat_bayi'] ?> Kg
        </td>
    </tr>
    <tr>
        <th width="20%">Panjang Bayi</th>
        <td width="1%">:</td>
        <td>
            <?= $data_lahir[0]['panjang_bayi'] ?> Cm
        </td>
    </tr>
    <tr>
        <th width="20%">Lokasi Lahir</th>
        <td width="1%">:</td>
        <td>
            <?= $data_lahir[0]['lokasi_lahir'] ?>
        </td>
    </tr>
    <tr>
        <th width="20%">Penolong</th>
        <td width="1%">:</td>
        <td>
            <?= $data_lahir[0]['penolong'] ?>
        </td>
    </tr>
</table>


<?php include('../_partials/bottom.php') ?>
