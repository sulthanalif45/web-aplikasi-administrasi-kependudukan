<?php
require_once("../../assets/lib/fpdf/fpdf.php");
require_once("../../config/koneksi.php");

class PDF extends FPDF
{
    // Page header
    function Header()
    {
      // Logo
      $this->Image('../../assets/img/logosumedang.png',20,10);

      // Arial bold 15
      $this->SetFont('Times','B',15);
      // Move to the right
      // $this->Cell(60);
      // Title
      $this->Cell(308,8,'PEMERINTAH KABUPATEN SUMEDANG',0,1,'C');
      $this->Cell(308,8,'KECAMATAN CIMANGGUNG',0,1,'C');
      $this->Cell(308,8,'DESA CIHANJUANG',0,1,'C');
      $this->SetFont('Times','',13);
      $this->Cell(308,8,'Jl.Bunter No.1 Des.Cihanjuang, Kec.Cimanggung, Kab.Sumedang',0,1,'C');
      // Line break

      $this->SetFont('Times','BU',12);
      for ($i=0; $i < 10; $i++) {
          $this->Cell(308,0,'',1,1,'C');
      }

      $this->Ln(5);

        $this->Cell(308,8,'LAPORAN DATA KELAHIRAN',0,1,'C');
        $this->Ln(2);

        $this->SetFont('Times','B',9.5);

        // header tabel
        $this->cell(10,7,'NO.',1,0,'C');
        $this->cell(30,7,'TANGGAL LAHIR',1,0,'C');
        $this->cell(75,7,'NAMA BAYI',1,0,'C');
        $this->cell(30,7,'JENIS KELAMIN',1,0,'C');
        $this->cell(20,7,'BERAT',1,0,'C');
        $this->cell(20,7,'PANJANG',1,0,'C');
        $this->cell(50,7,'NAMA AYAH',1,0,'C');
        $this->cell(50,7,'NAMA IBU',1,0,'C');
        $this->cell(25,7,'TEMPAT LAHIR',1,1,'C');

    }

    // Page footer
    function Footer()
    {
    	// Position at 1.5 cm from bottom
    	$this->SetY(-15);
    	// Arial italic 8
    	$this->SetFont('Arial','I',8);
    	// Page number
    	$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
}

// ambil dari database
$query = "SELECT * FROM tbl_kelahiran LEFT JOIN warga ON tbl_kelahiran.id_kelahiran = warga.id_warga";
$hasil = mysqli_query($db, $query);
$data_kelahiran = array();
while ($row = mysqli_fetch_assoc($hasil)) {
  $data_kelahiran[] = $row;
}

$pdf = new PDF('L', 'mm', [210, 330]);
$pdf->AliasNbPages();
$pdf->AddPage();

// set font
$pdf->SetFont('Times','',9);

// set penomoran
$nomor = 1;

foreach ($data_kelahiran as $kelahiran) {

    // hitung anggota
    $query_jumlah_anggota = "SELECT COUNT(*) AS total FROM warga_has_kartu_keluarga WHERE id_keluarga = ".$kelahiran['id_keluarga'];
    $hasil_jumlah_anggota = mysqli_query($db, $query_jumlah_anggota);
    $jumlah_jumlah_anggota = mysqli_fetch_assoc($hasil_jumlah_anggota);

    $pdf->cell(10, 7, $nomor++ . '.', 1, 0, 'C');
    $pdf->cell(30, 7, strtoupper($kelahiran['tgl_kelahiran']), 1, 0, 'C');
    $pdf->cell(75, 7, strtoupper($kelahiran['nama_bayi']), 1, 0, 'L');
    $pdf->cell(30, 7, strtoupper($kelahiran['jk']), 1, 0, 'C');
    $pdf->cell(20, 7, strtoupper($kelahiran['berat_bayi']).' kg', 1, 0, 'C');
    $pdf->cell(20, 7, strtoupper($kelahiran['panjang_bayi']).' cm', 1, 0, 'C');
    $pdf->cell(50, 7, strtoupper($kelahiran['nama_ayah']), 1, 0, 'L');
    $pdf->cell(50, 7, strtoupper($kelahiran['nama_ibu']), 1, 0, 'L');
    $pdf->cell(25, 7, strtoupper($kelahiran['tempat_lahir']), 1, 1, 'C');

}

	$pdf->Ln(10);

$pdf->Output();
?>
