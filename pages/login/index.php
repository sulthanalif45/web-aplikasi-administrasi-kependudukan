<?php
session_start();

// jika sudah login, alihkan ke halaman dasbor
if (isset($_SESSION['user'])) {
  header('Location: ../dasbor/index.php');
  exit();
}
?>

<?php include('../_partials/top-login.php') ?>

<body style="background-image:url(../../assets/img/blue.jpg);background-size:cover;">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                            <h3 class="panel-heading"><center>PROGRAM SISTEM INFORMASI ADMINISTRASI DATA KEPENDUDUKAN</center></h3>
                        </div>
                    </div>
                

                    <div class="panel-heading">
                        <h3 class="panel-title">Silahkan masukan username dan password untuk login !</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" action="../login/proses-login.php">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="username_user" type="username" required="" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password_user" type="password" value="" required="">
                                </div>
                                <button type="submit" class="btn btn-lg btn-primary btn-block">Masuk </button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
</body>
<?php include('../_partials/bottom-login.php') ?>
