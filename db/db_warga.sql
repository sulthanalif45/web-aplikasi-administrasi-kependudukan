-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Feb 2022 pada 05.32
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_warga`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kartu_keluarga`
--

CREATE TABLE `kartu_keluarga` (
  `id_keluarga` int(11) NOT NULL,
  `nomor_keluarga` varchar(16) NOT NULL,
  `id_kepala_keluarga` int(11) DEFAULT NULL,
  `alamat_keluarga` text NOT NULL,
  `dusun_keluarga` varchar(20) NOT NULL,
  `desa_kelurahan_keluarga` varchar(30) NOT NULL,
  `kecamatan_keluarga` varchar(30) NOT NULL,
  `kabupaten_kota_keluarga` varchar(30) NOT NULL,
  `provinsi_keluarga` varchar(30) NOT NULL,
  `negara_keluarga` varchar(30) NOT NULL,
  `rt_keluarga` varchar(3) NOT NULL,
  `rw_keluarga` varchar(3) NOT NULL,
  `kode_pos_keluarga` varchar(5) NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kartu_keluarga`
--

INSERT INTO `kartu_keluarga` (`id_keluarga`, `nomor_keluarga`, `id_kepala_keluarga`, `alamat_keluarga`, `dusun_keluarga`, `desa_kelurahan_keluarga`, `kecamatan_keluarga`, `kabupaten_kota_keluarga`, `provinsi_keluarga`, `negara_keluarga`, `rt_keluarga`, `rw_keluarga`, `kode_pos_keluarga`, `id_user`, `created_at`) VALUES
(11, '3208091601700010', 2, 'SUMEDANG', '', 'Cihanjuang', 'Cimanggung', 'Sumedang', 'Jawa Barat', 'Indonesia', '1', '1', '45515', 1, '2022-01-28 15:26:47'),
(20, '3208091601700013', 9, 'Ciawi', '', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'Indonesia', '01', '01', '4555', 1, '2022-01-28 15:27:56'),
(21, '3208091601700012', 3, 'SUMEDANG', '', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'Indonesia', '01', '01', '45515', 1, '2022-01-28 15:28:25'),
(22, '3208091601700014', 5, 'SUMEDANG', '', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'Indonesia', '01', '01', '45515', 1, '2022-01-28 15:29:09'),
(30, '3208091601700016', 30, '-', '', '-', '-', '-', '-', '-', '-', '-', '-', 1, '2022-01-28 15:29:31'),
(31, '3208091601700018', 34, 'Windujanten', '', '-', '-', '-', '-', '-', '-', '-', '-', 1, '2022-01-28 15:30:38'),
(32, '3208091601700015', 35, 'Cijoho', '', '-', '-', '-', '-', '-', '-', '-', '-', 13, '2018-01-17 17:53:06'),
(33, '3208091601700086', 36, 'Cipacing', '', 'Cipacing', 'Jatinangor', 'Sumedang', 'Jawa Barat', 'Indonesia', '2', '2', '43442', 1, '2022-01-28 15:31:02'),
(34, '3208091601712521', 40, 'Perum SBG', '', 'Cihanjuang', 'Cimanggung', 'Sumedang', 'Jawa Barat', 'Indonesia', '03', '01', '45364', 1, '2022-02-15 15:05:05'),
(35, '3208091601712162', 10, 'Bunter', 'Bunter', 'Cihanjuang', 'Cimanggung', 'Sumedang', 'Jawa Barat', 'Indonesia', '03', '01', '45364', 1, '2022-02-15 17:05:17'),
(36, '3208091601712105', 11, 'SBG', 'SBG', 'Cihanjuang', 'Cimanggung', 'Sumedang', 'Jawa Barat', 'Indonesia', '02', '01', '45364', 1, '2022-02-15 17:08:27'),
(38, '3208091602342', 43, 'Jalan Kenangan 25', '', '-', '-', '-', '-', '-', '-', '-', '-', 1, '2022-02-15 17:28:18'),
(39, '320809164563', 44, 'Cikijing jengkol dua', '', '-', '-', '-', '-', '-', '-', '-', '-', 1, '2022-02-16 07:49:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mutasi_keluar`
--

CREATE TABLE `mutasi_keluar` (
  `id_mutasi` int(11) NOT NULL,
  `id_warga` int(11) NOT NULL,
  `alamat_mutasi` text NOT NULL,
  `desa_kelurahan_mutasi` varchar(20) NOT NULL,
  `kabupaten_kota_mutasi` varchar(20) NOT NULL,
  `provinsi_mutasi` varchar(20) NOT NULL,
  `negara_mutasi` varchar(20) NOT NULL,
  `dusun_mutasi` varchar(30) NOT NULL,
  `rt_mutasi` varchar(3) NOT NULL,
  `rw_mutasi` varchar(3) NOT NULL,
  `kode_pos_mutasi` varchar(10) NOT NULL,
  `tanggal_pindah` date NOT NULL,
  `alasan_pindah` varchar(20) NOT NULL,
  `jenis_pindah` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `kecamatan_mutasi` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mutasi_keluar`
--

INSERT INTO `mutasi_keluar` (`id_mutasi`, `id_warga`, `alamat_mutasi`, `desa_kelurahan_mutasi`, `kabupaten_kota_mutasi`, `provinsi_mutasi`, `negara_mutasi`, `dusun_mutasi`, `rt_mutasi`, `rw_mutasi`, `kode_pos_mutasi`, `tanggal_pindah`, `alasan_pindah`, `jenis_pindah`, `created_at`, `kecamatan_mutasi`) VALUES
(10, 16, '-', '-', '-', '-', '-', '-', '-', '-', '-', '2018-01-03', 'Kesehatan', 'Anggota Keluarga', '2018-01-03 14:08:57', '-'),
(11, 14, 'Bandung', 'Cinta', 'Jakarta Selatan', 'DKI Jakarta', 'Indonesia', 'Rawa Rangun', '002', '012', '23124245', '2022-01-25', 'Kesehatan', 'Anggota Keluarga', '2022-01-27 14:06:08', 'Kenangan'),
(12, 44, 'Cikijing jalan indah', 'Cinta', 'Jakarta Selatan', 'DKI Jakarta', 'Indonesia', 'Rawa Rangun', '002', '012', '45364', '2022-02-24', 'Pekerjaan', 'Anggota Keluarga', '2022-02-16 07:56:24', 'Kenangan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mutasi_masuk`
--

CREATE TABLE `mutasi_masuk` (
  `id_mutasi_masuk` int(11) NOT NULL,
  `id_warga` int(11) NOT NULL,
  `id_keluarga` int(11) NOT NULL,
  `dusun_masuk` varchar(30) NOT NULL,
  `rt_masuk` varchar(3) NOT NULL,
  `rw_masuk` varchar(3) NOT NULL,
  `alamat_asal` text NOT NULL,
  `tanggal_pindah` date NOT NULL,
  `alasan_pindah` varchar(20) NOT NULL,
  `jenis_kepindahan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mutasi_masuk`
--

INSERT INTO `mutasi_masuk` (`id_mutasi_masuk`, `id_warga`, `id_keluarga`, `dusun_masuk`, `rt_masuk`, `rw_masuk`, `alamat_asal`, `tanggal_pindah`, `alasan_pindah`, `jenis_kepindahan`) VALUES
(17, 30, 23, 'Tarikolot', '01', '01', '-', '2018-01-04', 'Keluarga', 'Anggota Keluarga'),
(25, 34, 31, 'Tarikolot', '02', '01', 'Windujanten', '2018-01-18', 'Keluarga', 'Anggota Keluarga'),
(26, 35, 32, 'Dukuh', '06', '01', 'Cijoho', '2018-01-18', 'Keluarga', 'Anggota Keluarga'),
(31, 43, 38, 'Bunter', '02', '01', 'Jalan Kenangan 25', '2022-02-16', 'Pekerjaan', 'Anggota Keluarga'),
(32, 44, 39, 'Bunter', '02', '01', 'Cikijing jengkol dua', '2022-02-16', 'Pendidikan', 'Anggota Keluarga');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kelahiran`
--

CREATE TABLE `tbl_kelahiran` (
  `id_kelahiran` int(11) NOT NULL,
  `tgl_kelahiran` datetime NOT NULL,
  `nama_bayi` varchar(50) NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `berat_bayi` varchar(10) DEFAULT NULL,
  `panjang_bayi` int(10) DEFAULT NULL,
  `nama_ayah` varchar(100) DEFAULT NULL,
  `nama_ibu` varchar(100) DEFAULT NULL,
  `lokasi_lahir` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `id_keluarga` int(11) DEFAULT NULL,
  `penolong` varchar(25) NOT NULL,
  `id_warga` int(11) DEFAULT NULL,
  `id_surat` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_kelahiran`
--

INSERT INTO `tbl_kelahiran` (`id_kelahiran`, `tgl_kelahiran`, `nama_bayi`, `jk`, `berat_bayi`, `panjang_bayi`, `nama_ayah`, `nama_ibu`, `lokasi_lahir`, `tempat_lahir`, `id_keluarga`, `penolong`, `id_warga`, `id_surat`) VALUES
(2, '2017-12-21 00:00:00', 'Dejan Kecil', 'L', '3', 55, 'Dejan', 'Entah', 'Rumah Bersalin', 'SUMEDANG', 11, 'Bidan cantik', 10, 1),
(3, '2017-12-22 00:00:00', 'Cihiro', 'P', '2,5', 55, 'Dejan', 'Entah', 'Bidan', 'SUMEDANG', 22, '', NULL, 2),
(27, '2018-01-02 00:00:00', 'Yoshino', 'L', '3', 50, 'DD', 'MM', 'Bukan Rumah Bersalin', 'sumedang', 14, 'Entah', 12, 3),
(28, '2018-01-02 00:00:00', 'Yoshino', 'L', '3', 50, 'DD', 'MM', 'Bukan Rumah Bersalin', 'sumedang', 12, 'Entah', 13, 4),
(29, '2018-01-02 00:00:00', 'Yoshino yosuke', 'L', '3', 55, 'DD', 'DDD', 'Bukan Rumah Bersalin', 'sumedang', 14, 'Bidan cantik', 14, 5),
(30, '2018-01-02 00:00:00', 'Yoshino yosuke 2', 'L', '3', 55, 'DD', 'DDD', 'Bukan Rumah Bersalin', 'sumedang', 10, 'Bidan cantik', 15, 6),
(31, '2018-01-03 00:00:00', 'suryani', 'L', '3', 50, 'dejan', 'entah', 'Rumah Bersalin', 'sumedang', 14, 'Bidan cantik', 16, 7),
(32, '2018-01-04 00:00:00', 'Yani Suryani', 'L', '4', 55, 'Dejan', 'Duka', 'Rumah Bersalin', 'SUMEDANG', 10, 'Bidan cantik', 31, 8),
(34, '2022-01-29 00:00:00', 'Deriq', 'L', '3', 30, 'Thoriq', 'Dewi', 'Rumah Bersalin', 'Sumedang', 2147483647, 'Drs. Rizal', 39, 9),
(35, '2022-02-15 00:00:00', 'Sulthan jr', 'L', '2.5', 25, 'Sulthan Alif Hayatyo', 'Ulfah Salsabila', 'Rumah Bersalin', 'Sumedang', 2147483647, 'Dr. Wildan', 41, 35);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_meninggal`
--

CREATE TABLE `tbl_meninggal` (
  `id_meninggal` int(11) NOT NULL,
  `tgl_meninggal` datetime NOT NULL,
  `sebab` varchar(50) DEFAULT NULL,
  `id_warga` int(11) DEFAULT NULL,
  `tempat_kematian` varchar(100) DEFAULT NULL,
  `nama_pelapor` varchar(100) DEFAULT NULL,
  `alamat_pelapor` varchar(100) NOT NULL,
  `hubungan_pelapor` varchar(100) DEFAULT NULL,
  `id_surat` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_meninggal`
--

INSERT INTO `tbl_meninggal` (`id_meninggal`, `tgl_meninggal`, `sebab`, `id_warga`, `tempat_kematian`, `nama_pelapor`, `alamat_pelapor`, `hubungan_pelapor`, `id_surat`) VALUES
(1, '2017-12-27 00:00:00', '-', 1, 'RS', 'Amin', '', 'Saudara', 1),
(6, '2018-01-02 00:00:00', 'Sakit', 11, 'RS', 'pelapor', '', 'Duka', 2),
(7, '2018-01-02 00:00:00', 'Sakit', 13, '-', 'pelapor', '', 'Duka', 3),
(16, '2022-01-25 00:00:00', 'tertabrak entog', 9, 'pacilingan', 'sulthan', 'sbg', 'gaada', 4),
(17, '2022-01-29 00:00:00', 'Tabrak Lari', 29, 'Depan Masoem', 'Hilman', 'Cipacing', 'Teman', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(45) NOT NULL,
  `username_user` varchar(20) NOT NULL,
  `password_user` varchar(32) NOT NULL,
  `keterangan_user` text NOT NULL,
  `status_user` enum('Admin','Kasi_Pemerintahan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `username_user`, `password_user`, `keterangan_user`, `status_user`) VALUES
(1, 'Sulthan', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin di aplikasi pendataan warga', 'Admin'),
(11, 'Kepala Desa', 'Admin', 'e3afed0047b08059d0fada10f400c1e5', '', 'Admin'),
(13, 'Kasi Pemerintahan', 'kasi_pemerintahan', '827ccb0eea8a706c4c34a16891f84e7b', 'Kasi Pemerintahan', 'Kasi_Pemerintahan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `warga`
--

CREATE TABLE `warga` (
  `id_warga` int(11) NOT NULL,
  `nik_warga` varchar(16) NOT NULL,
  `nama_warga` varchar(45) NOT NULL,
  `tempat_lahir_warga` varchar(30) NOT NULL,
  `tanggal_lahir_warga` date NOT NULL,
  `jenis_kelamin_warga` enum('L','P') NOT NULL,
  `alamat_ktp_warga` text NOT NULL,
  `alamat_warga` text NOT NULL,
  `desa_kelurahan_warga` varchar(30) NOT NULL,
  `kecamatan_warga` varchar(30) NOT NULL,
  `kabupaten_kota_warga` varchar(30) NOT NULL,
  `provinsi_warga` varchar(30) NOT NULL,
  `negara_warga` varchar(30) NOT NULL,
  `dusun_warga` enum('SBG','Bunter') NOT NULL,
  `rt_warga` varchar(3) NOT NULL,
  `rw_warga` varchar(3) NOT NULL,
  `agama_warga` enum('Islam','Kristen','Katholik','Hindu','Budha','Konghucu') NOT NULL,
  `pendidikan_terakhir_warga` varchar(20) NOT NULL,
  `pekerjaan_warga` varchar(20) NOT NULL,
  `status_warga` enum('Tinggal Tetap','Meninggal','Pindah Datang','Pindah Keluar') NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `warga`
--

INSERT INTO `warga` (`id_warga`, `nik_warga`, `nama_warga`, `tempat_lahir_warga`, `tanggal_lahir_warga`, `jenis_kelamin_warga`, `alamat_ktp_warga`, `alamat_warga`, `desa_kelurahan_warga`, `kecamatan_warga`, `kabupaten_kota_warga`, `provinsi_warga`, `negara_warga`, `dusun_warga`, `rt_warga`, `rw_warga`, `agama_warga`, `pendidikan_terakhir_warga`, `pekerjaan_warga`, `status_warga`, `id_user`, `created_at`) VALUES
(1, '3208090101960089', 'Sulaiman', 'CIHANJUANG', '1985-02-26', 'L', 'CIHANJUANG', 'CIHANJUANG', 'Dukuh', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'Indonesia', 'SBG', '001', '002', 'Islam', 'SD', 'Petani', 'Meninggal', 1, '2022-01-27 16:37:51'),
(2, '3208090511780005', 'Anang', 'SUMEDANG', '1978-11-05', 'L', 'CIHANJUANG', 'CIHANJUANG', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'Indonesia', 'SBG', '02', '01', 'Islam', 'SMP', 'pedagang', 'Tinggal Tetap', 11, '2022-01-22 17:26:32'),
(3, '3208090511784362', 'Denis', 'SUMEDANG', '2017-12-19', 'L', 'Perum SBG', 'Perum SBG', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'Indonesia', 'SBG', '02', '01', 'Kristen', 'SMA', 'Mahasiswa', 'Tinggal Tetap', 11, '2022-01-28 05:35:50'),
(4, '3208090511764286', 'Ulfah Salsabila', 'Jakarta', '2006-12-05', 'P', 'Sawahdadap', 'Sawahdadap', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'Indonesia', 'Bunter', '03', '01', 'Islam', 'SMA', 'Mahasiswa', 'Tinggal Tetap', 11, '2022-01-28 15:19:11'),
(5, '3208090511792837', 'Sarwenda', 'Kebumen', '2001-04-04', 'P', 'Perum Alam Asri', 'Perum Alam Asri', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'Indonesia', 'Bunter', '05', '01', 'Islam', 'SMA', 'Mahasiswa', 'Tinggal Tetap', 11, '2022-01-28 16:22:45'),
(7, '3208090511729165', 'Malika', 'Brebes', '2017-12-17', 'P', '-', '-', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'Indonesia', 'SBG', '01', '01', 'Islam', 'SMA', 'Mahasiswa', 'Tinggal Tetap', 11, '2022-01-28 15:22:20'),
(9, '3208090511702975', 'Dudung', 'SUMEDANG', '2017-12-17', 'L', 'Linggar', 'Linggar', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'Indonesia', 'Bunter', '01', '01', 'Islam', 'SMP', 'pedagang', 'Meninggal', 11, '2022-01-28 15:33:28'),
(10, '3208090511782648', 'Jajang', 'Bandung', '1996-01-01', 'L', 'Windusengkahan', 'Windusengkahan', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'Indonesia', 'SBG', '06', '01', 'Islam', 'SMA', 'Mahasiswa', 'Tinggal Tetap', 11, '2022-01-28 15:32:48'),
(11, '3208090511763008', 'Baim', 'sumedang', '2017-12-29', 'L', 'Ciawi', 'Ciawi', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'INDONESIA', 'Bunter', '01', '01', 'Islam', 'Tidak Sekolah', '-', 'Meninggal', 1, '2022-02-15 17:12:04'),
(12, '3208090511700094', 'Yoshino', 'sumedang', '2018-01-02', 'L', 'SUMEDANG', 'SUMEDANG', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'INDONESIA', 'SBG', '01', '05', 'Islam', 'Tidak Sekolah', '-', 'Meninggal', 0, '2022-01-28 15:34:11'),
(13, '5a4b0bc3c0e9b', 'Yoshino', 'sumedang', '2018-01-02', 'L', '', 'SUMEDANG', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'INDONESIA', '', '01', '01', 'Islam', '-', '-', 'Tinggal Tetap', 0, '2022-01-22 17:26:32'),
(14, '3208090511766483', 'Yoshino yosuke', 'sumedang', '2018-01-02', 'L', 'SUMEDANG', 'SUMEDANG', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'INDONESIA', 'SBG', '01', '05', 'Islam', 'SMP', '-', 'Pindah Keluar', 0, '2022-01-28 15:39:06'),
(15, '2311', 'Yoshino yosuke 2', 'sumedang', '2018-01-02', 'L', 'SUMEDANG', 'SUMEDANG', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'INDONESIA', 'Bunter', '01', '02', 'Islam', '-', '-', 'Tinggal Tetap', 0, '2022-01-29 09:45:43'),
(16, '3208090511777364', 'Suryani', 'Sumedang', '2018-01-03', 'L', 'SUMEDANG', 'SUMEDANG', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'INDONESIA', 'Bunter', '01', '05', 'Islam', 'SD', '-', 'Pindah Keluar', 0, '2022-01-28 15:39:44'),
(17, '9988', 'Asuy', 'SUMEDANG', '2018-01-03', 'L', '', '', 'CIHANJUANG', 'Ciawi Gebang', 'SUMEDANG', 'Jawa Barat', 'Indonesia', 'Bunter', '01', '01', 'Islam', 'Tidak Sekolah', 'pedagang', 'Tinggal Tetap', 13, '2022-01-28 16:24:13'),
(28, '5a4d2b4cdb7c5', 'aaaaa', 'SUMEDANG', '2018-01-04', 'L', '', '', '-', '-', '-', '-', '-', 'Bunter', '-', '-', 'Katholik', '-', '-', 'Tinggal Tetap', 13, '2022-01-28 16:23:52'),
(29, '5a4d2b964b148', 'aaaaa', 'SUMEDANG', '2018-01-04', 'L', '', '', '-', '-', '-', '-', '-', 'Bunter', '-', '-', 'Katholik', '-', '-', 'Meninggal', 13, '2022-01-29 09:19:47'),
(30, '3208090511700937', 'Badu', 'SUMEDANG', '2018-01-04', 'L', 'Cikarang', 'Cikarang', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'JAWA BARAT', 'INDONESIA', 'Bunter', '02', '01', 'Katholik', 'SMA', 'Pekerja Swasta', 'Pindah Datang', 13, '2022-01-28 15:35:53'),
(31, '5a4db39e707b3', 'Yani Suryani', 'SUMEDANG', '2018-01-04', 'L', 'SUMEDANG', 'SUMEDANG', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'INDONESIA', '', '01', '02', 'Islam', '-', '-', 'Tinggal Tetap', 0, '2018-01-04 04:54:54'),
(34, '3208090511780005', 'Darmawan', 'SUMEDANG', '1970-01-13', 'L', 'Windujanten', 'Windujanten', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'Jawa Barat', 'Indonesia', '', '02', '01', 'Islam', 'SMP', 'Wiraswasta', 'Pindah Datang', 13, '2018-01-17 17:55:18'),
(35, '3208090511774659', 'Dasuki', 'Cijoho', '1968-03-05', 'L', 'Cijoho', 'Cijoho', 'CIHANJUANG', 'CIMANGGUNG', 'SUMEDANG', 'JAWA BARAT', 'INDONESIA', 'Bunter', '06', '01', 'Hindu', 'SMP', '-', 'Pindah Datang', 13, '2022-01-28 15:36:50'),
(36, '3208090511700001', 'Thoriq', 'Boyolali', '2022-01-24', 'L', 'Boyolali', 'Cipacing', 'Cihanjuang', 'Cimanggung', 'Sumedang', 'Jawa Barat', 'Indonesia', 'Bunter', '02', '01', 'Katholik', 'SMP', 'Pengacara', 'Tinggal Tetap', 1, '2022-01-28 15:37:11'),
(37, '1241342', 'Rizal', 'Majalaya', '2003-01-08', 'L', 'Majalaya', 'Majalaya', 'Cihanjuang', 'Cimanggung', 'Sumedang', 'Jawa Barat', 'Indonesia', 'Bunter', '04', '01', 'Katholik', 'Tidak Tamat SD', 'Pengacara', 'Tinggal Tetap', 1, '2022-01-24 21:11:09'),
(38, '3208090511700263', 'Dewi', 'Majalaya', '1991-01-25', 'P', 'Majalaya', 'Perum Majalaya', 'Cihanjuang', 'Cimanggung', 'Sumedang', 'Jawa Barat', 'Indonesia', 'Bunter', '04', '01', 'Islam', 'SMP', 'Ibu Rumah Tangga', 'Tinggal Tetap', 1, '2022-01-29 08:48:00'),
(39, '3208090511739995', 'Deriq', 'Sumedang', '2022-01-29', 'L', 'Cipacing', 'Cipacing', 'Cihanjuang', 'CIMANGGUNG', 'SUMEDANG', 'JAWA BARAT', 'INDONESIA', 'Bunter', '01', '02', '', '-', '-', 'Tinggal Tetap', 0, '2022-01-29 09:02:26'),
(40, '3208090587384', 'Sulthan Alif Hayatyo', 'Sumedang', '2001-01-09', 'L', 'Perum SBG Jl.BA 3 No.11', 'Perum SBG Jl.BA 3 No.11', 'Cihanjuang', 'Cimanggung', 'Sumedang', 'Jawa Barat', 'Indonesia', 'SBG', '03', '01', 'Islam', 'SMA', 'Mahasiswa', 'Tinggal Tetap', 1, '2022-02-15 11:51:08'),
(41, '3208090511741299', 'Sulthan jr', 'Sumedang', '2022-02-15', 'L', 'Perum SBG', 'Perum SBG', 'Cihanjuang', 'CIMANGGUNG', 'SUMEDANG', 'JAWA BARAT', 'INDONESIA', 'SBG', '01', '03', '', '-', '-', 'Tinggal Tetap', 0, '2022-02-15 16:43:22'),
(43, '3208091602342', 'Bobi', 'Jakarta', '2022-02-16', 'L', 'Jalan Kenangan 25', 'Jalan Kenangan 25', '-', '-', '-', '-', '-', 'Bunter', '02', '01', '', '-', '-', 'Pindah Datang', 1, '2022-02-15 17:28:17'),
(44, '320809164563', 'Lastri', 'Cikijing', '1991-02-13', 'L', 'Cikijing jengkol dua', 'Cikijing jengkol dua', '-', '-', '-', '-', '-', 'Bunter', '02', '01', '', '-', '-', 'Pindah Keluar', 1, '2022-02-16 07:56:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `warga_has_kartu_keluarga`
--

CREATE TABLE `warga_has_kartu_keluarga` (
  `id_warga` int(11) NOT NULL,
  `id_keluarga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `warga_has_kartu_keluarga`
--

INSERT INTO `warga_has_kartu_keluarga` (`id_warga`, `id_keluarga`) VALUES
(0, 0),
(1, 11),
(2, 11),
(12, 11),
(15, 11),
(31, 11),
(7, 20),
(9, 20),
(10, 20),
(3, 21),
(13, 21),
(14, 22),
(16, 22),
(36, 33),
(37, 33),
(38, 33),
(39, 33),
(4, 34),
(40, 34),
(41, 34),
(11, 36);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kartu_keluarga`
--
ALTER TABLE `kartu_keluarga`
  ADD PRIMARY KEY (`id_keluarga`),
  ADD UNIQUE KEY `FK_keluarga_penduduk` (`id_kepala_keluarga`) USING BTREE,
  ADD UNIQUE KEY `id_kepala_keluarga` (`id_kepala_keluarga`) USING BTREE,
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `mutasi_keluar`
--
ALTER TABLE `mutasi_keluar`
  ADD PRIMARY KEY (`id_mutasi`),
  ADD KEY `id_warga` (`id_warga`);

--
-- Indeks untuk tabel `mutasi_masuk`
--
ALTER TABLE `mutasi_masuk`
  ADD PRIMARY KEY (`id_mutasi_masuk`),
  ADD KEY `id_warga` (`id_warga`),
  ADD KEY `id_keluarga` (`id_keluarga`);

--
-- Indeks untuk tabel `tbl_kelahiran`
--
ALTER TABLE `tbl_kelahiran`
  ADD PRIMARY KEY (`id_kelahiran`),
  ADD KEY `id_surat` (`id_surat`),
  ADD KEY `id_warga` (`id_warga`),
  ADD KEY `id_keluarga` (`id_keluarga`);

--
-- Indeks untuk tabel `tbl_meninggal`
--
ALTER TABLE `tbl_meninggal`
  ADD PRIMARY KEY (`id_meninggal`),
  ADD KEY `id_surat` (`id_surat`),
  ADD KEY `id_warga` (`id_warga`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indeks untuk tabel `warga`
--
ALTER TABLE `warga`
  ADD PRIMARY KEY (`id_warga`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `warga_has_kartu_keluarga`
--
ALTER TABLE `warga_has_kartu_keluarga`
  ADD UNIQUE KEY `id_warga` (`id_warga`),
  ADD KEY `id_penduduk` (`id_warga`,`id_keluarga`),
  ADD KEY `warga_has_kartu_keluarga_ibfk_2` (`id_keluarga`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kartu_keluarga`
--
ALTER TABLE `kartu_keluarga`
  MODIFY `id_keluarga` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT untuk tabel `mutasi_keluar`
--
ALTER TABLE `mutasi_keluar`
  MODIFY `id_mutasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `mutasi_masuk`
--
ALTER TABLE `mutasi_masuk`
  MODIFY `id_mutasi_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT untuk tabel `tbl_kelahiran`
--
ALTER TABLE `tbl_kelahiran`
  MODIFY `id_kelahiran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT untuk tabel `tbl_meninggal`
--
ALTER TABLE `tbl_meninggal`
  MODIFY `id_meninggal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `warga`
--
ALTER TABLE `warga`
  MODIFY `id_warga` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kartu_keluarga`
--
ALTER TABLE `kartu_keluarga`
  ADD CONSTRAINT `kartu_keluarga_ibfk_1` FOREIGN KEY (`id_kepala_keluarga`) REFERENCES `warga` (`id_warga`),
  ADD CONSTRAINT `kartu_keluarga_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `tbl_meninggal`
--
ALTER TABLE `tbl_meninggal`
  ADD CONSTRAINT `tbl_meninggal_ibfk_1` FOREIGN KEY (`id_warga`) REFERENCES `warga` (`id_warga`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
